import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:calculus/cubit/activity/activity_cubit.dart';

import 'package:calculus/ui/pages/game.dart';

import 'package:calculus/ui/parameters/parameter_painter_difficulty_level.dart';

class ApplicationConfig {
  // activity parameter: difficulty level values
  static const String parameterCodeDifficultyLevel = 'activity.difficultyLevel';
  static const String difficultyLevelValueEasy = 'easy';
  static const String difficultyLevelValueMedium = 'medium';
  static const String difficultyLevelValueHard = 'hard';
  static const String difficultyLevelValueNightmare = 'nightmare';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Calculus',
    activitySettings: [
      // difficulty level
      ApplicationSettingsParameter(
        code: parameterCodeDifficultyLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueEasy,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueHard,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueNightmare,
            color: Colors.purple,
          ),
        ],
        itemsPerLine: 2,
        customPainter: (context, value) => ParameterPainterDifficultyLevel(
          context: context,
          value: value,
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
