import 'package:flutter/material.dart';

import 'package:calculus/ui/widgets/indicators/indicator_score.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ScoreIndicator(),
      ],
    );
  }
}
