import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:calculus/cubit/activity/activity_cubit.dart';
import 'package:calculus/models/activity/activity.dart';

class ScoreIndicator extends StatelessWidget {
  const ScoreIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final int score = currentActivity.score;

        const Color textColor = Color.fromARGB(255, 238, 238, 238);
        const Color outlineColor = Color.fromARGB(255, 200, 200, 200);

        return OutlinedText(
          text: '$score',
          fontSize: 50,
          textColor: textColor,
          outlineColor: outlineColor,
        );
      },
    );
  }
}
